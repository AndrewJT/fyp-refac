package uk.andrewtowers.routevis.data;

import uk.andrewtowers.routevis.app.App;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RouteVisProject {

    private Journey[] journeys;
    private Redeployment[] redeployments;
    private String[] timeSet;
    private List<Path> paths;

    private App app;

    private File redeploymentFile;

    //Finds a journey code in the list of journeys
    //Returns null if there isn't one
    public Journey getJourneyFromCode(String journeyCode){

        for (Journey j : journeys) {
            if(j.getJourneyCode().equalsIgnoreCase(journeyCode)){
                return j;
            }
        }
        System.err.println("Journey code: '" + journeyCode + "' not found!");
        return null;

    }

    public Journey[] getJourneys() {
        return journeys;
    }

    public Path[] getPaths() {
        return paths.toArray(new Path[0]);
    }

    public void addPath(Path p){
        p.setPathIndex(paths.size());
        paths.add(p);
    }

    public void addPathToTable(Path p){
        Object[] row = {p.getPathIndex(), (p.getJourneys().length - 2), p.getEcsRedeployments(), "View"};
        app.windowUI.pathsTableModel.addRow(row);
    }

    public void setJourneys(Journey[] journeys) {
        this.journeys = journeys;
    }

    public Redeployment[] getRedeployments(){
        return redeployments;
    }

    public void setRedeployments(Redeployment[] newRedep) {

        if(journeys.length == 0){
            System.err.println("Trying to set redeployments before journeys! This is not allowed!");
            return;
        }
        this.redeployments = newRedep;
    }

    public String[] getTimeSet() {
        return timeSet;
    }

    public void setTimeSet(String[] timeSet) {
        this.timeSet = timeSet;
    }

    public RouteVisProject(App app){
        this.paths = new ArrayList();
        this.app = app;
    }

    public File getRedeploymentFile() {
        return redeploymentFile;
    }

    public void setRedeploymentFile(File redeploymentFile) {
        this.redeploymentFile = redeploymentFile;
    }

    @Override
    public String toString(){
        return String.format("Project with %d journeys and %d redeployments", this.journeys.length, this.redeployments.length);
    }
}
