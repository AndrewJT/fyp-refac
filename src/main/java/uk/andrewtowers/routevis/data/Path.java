package uk.andrewtowers.routevis.data;

import org.gephi.graph.api.*;
import org.openide.util.Lookup;
import uk.andrewtowers.routevis.app.App;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Path {

    private Journey[] journeys;
    private Redeployment[] redeployments;

    private int pathIndex;

    private int[] equip;
    private int noUnits;

    public Path(Journey[] journeys, Redeployment[] redeployments, int noUnits, int[] equip, App app) {
        this.journeys = journeys;
        this.redeployments = redeployments;
        this.noUnits = noUnits;
        this.equip = equip;
        this.pathIndex = -1;
    }

    public Path(Journey[] journeys, Redeployment[] redeployments, int equip, App app) {
        this.journeys = journeys;
        this.redeployments = redeployments;
        this.noUnits = 1;
        this.equip = new int[]{equip};
        this.pathIndex = -1;
    }

    public void drawPath(App app){
        drawPath(Color.orange,5f, app);
        app.rtProj.addPathToTable(this);
    }

    @Override
    public String toString(){
        String text = "Path journeys: [";
        for (Journey j : journeys){
            text += j.getIndex() + ", ";
        }
        text += "] path redeployments [";
        for (Redeployment r : redeployments){
            text += r.getArcIndex() + ", ";
        }
        return text + "] ";
    }

    public void drawPath(Color nodeColor, float edgeWeight, App app){

        Node[] graphNodes = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getNodes().toArray();
        Edge[] graphEdges = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getEdges().toArray();

        System.out.println("Drawing path: " + this.toString());
        int i = 0;
        while(i < (journeys.length - 1)){

            graphNodes[journeys[i].getIndex()].setAlpha(1f);
            graphNodes[journeys[i].getIndex()].setColor(nodeColor);

            //System.out.println("Redeployment " + redeployments[i]);
            //System.out.println("Act edge" + graphEdges[redeployments[i].getEdgeIndex()].getLabel());
            graphEdges[redeployments[i].getEdgeIndex()].setAlpha(1f);
            graphEdges[redeployments[i].getEdgeIndex()].setWeight(edgeWeight);

            //Edge colour is based on no of paths
            int nopaths = (int)graphEdges[redeployments[i].getEdgeIndex()].getAttribute(app.graphDrawer.no_paths);
            if(nopaths == 1){
                graphEdges[redeployments[i].getEdgeIndex()].setColor(new Color(0, 255, 234));
            }else if(nopaths == 2){
                graphEdges[redeployments[i].getEdgeIndex()].setColor(new Color(0, 117, 255));
            }else if(nopaths == 3){
                graphEdges[redeployments[i].getEdgeIndex()].setColor(new Color(0, 30, 97));
            } else {
                graphEdges[redeployments[i].getEdgeIndex()].setColor(new Color(255, 0, 0));
            }

            i++;
        }

        //Now add the sink node
        graphNodes[journeys[i].getIndex()].setAlpha(1f);
        graphNodes[journeys[i].getIndex()].setColor(nodeColor);

        app.previewController.refreshPreview();

    }

    public int getEcsRedeployments(){
        return 1;
    }

    public Journey[] getJourneys() {
        return journeys;
    }

    public Redeployment[] getRedeployments() {
        return redeployments;
    }

    public int[] getEquip() {
        return equip;
    }

    public int getPathIndex() {
        return pathIndex;
    }

    public void setPathIndex(int pathIndex) {
        this.pathIndex = pathIndex;
    }
}