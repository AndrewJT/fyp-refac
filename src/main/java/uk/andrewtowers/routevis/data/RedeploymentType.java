package uk.andrewtowers.routevis.data;

public enum RedeploymentType {

    SIGNON{
        public String toString(){
            return "Sign on";
        }
    },
    SAMELOC{
        public String toString(){
            return "Same location";
        }
    },
    ECS{
        public String toString(){
            return "Empty Coaching Stock";
        }
    },
    SIGNOFF{
        public String toString(){
            return "Sign off";
        }
    };


    public static RedeploymentType typeFromString(String s){
        if(s.equalsIgnoreCase("sign-off") || s.equalsIgnoreCase("Sign off")){
            return RedeploymentType.SIGNOFF;
        } else if(s.equalsIgnoreCase("trip-sameloc") || s.equalsIgnoreCase("Same location")){
            return RedeploymentType.SAMELOC;
        } else if(s.equalsIgnoreCase("sign-on") || s.equalsIgnoreCase("Sign on")){
            return RedeploymentType.SIGNON;
        } else if(s.equalsIgnoreCase("trip-ecs") || s.equalsIgnoreCase("ecs") || s.equalsIgnoreCase("Empty Coaching Stock")){
            return RedeploymentType.ECS;
        }

        System.err.println("Invalid redeployment type " + s + " will continue as ECS!");
        return RedeploymentType.ECS;
    }
}
