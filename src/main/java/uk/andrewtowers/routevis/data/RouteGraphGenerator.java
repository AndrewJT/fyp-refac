package uk.andrewtowers.routevis.data;

import org.gephi.io.generator.spi.Generator;
import org.gephi.io.generator.spi.GeneratorUI;
import org.gephi.io.importer.api.*;
import org.gephi.utils.progress.ProgressTicket;
import org.openide.util.lookup.ServiceProvider;

import java.awt.*;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

@ServiceProvider(service = Generator.class)
public class RouteGraphGenerator implements Generator {

    protected Boolean cancel = false;
    protected ProgressTicket progress;

    private RouteVisProject rVProj;

    @Override
    public void generate(ContainerLoader containerLoader) {

        if(rVProj == null) {
            System.err.println("Project not set");
            cancel();
            return;
        }

        //This prevents gephi from merging parallel edges, which causes errors
        containerLoader.setEdgesMergeStrategy(EdgeMergeStrategy.NO_MERGE);

        //Create table columns
        //Node (Journey) columns
        containerLoader.addNodeColumn("journey_code", String.class);
        containerLoader.addNodeColumn("start_loc", String.class);
        containerLoader.addNodeColumn("start_time", Integer.class);
        containerLoader.addNodeColumn("end_loc", String.class);
        containerLoader.addNodeColumn("end_time", Integer.class);
        containerLoader.addNodeColumn("equipment", Integer.class);

        //Edge (Redeployment) columns
        containerLoader.addEdgeColumn("arc_index", int.class);
        containerLoader.addEdgeColumn("from_journey", String.class);
        containerLoader.addEdgeColumn("from_loc", String.class);
        containerLoader.addEdgeColumn("to_journey", String.class);
        containerLoader.addEdgeColumn("to_loc", String.class);
        containerLoader.addEdgeColumn("redeployment_type", String.class);
        containerLoader.addNodeColumn("equipment", Integer.class);
        containerLoader.addNodeColumn("no_paths", Integer.class);

        List <NodeDraft> nodes = new ArrayList<>();
        List <EdgeDraft> edges = new ArrayList<>();

        //Create a node for each journey
        int slots[] = new int[50];  //Used for determining y co-ords
        int lasthour = 0;
        float currentX = 0;

        for(Journey j : rVProj.getJourneys()) {

            NodeDraft n = containerLoader.factory().newNodeDraft(j.getJourneyCode());
            n.setLabel(j.toString());
            n.setSize(100f);
            n.setColor(Color.GREEN);

            //Set table attributes
            if(j.getStartLoc() == ""){
                n.setValue("start_loc", "n/a");
            } else {
                n.setValue("start_loc", j.getStartLoc());
            }

            n.setValue("journey_code", j.getJourneyCode());
            n.setValue("start_time", j.getStartInMins());
            n.setValue("end_loc", j.getEndLoc());
            n.setValue("end_time", j.getEndInMins());
            n.setValue("equipment", j.getEquip());

            int jStartHour = j.getStartTime()[0];

            if(jStartHour > lasthour){
                currentX += 2700f;
                lasthour = jStartHour;
            }
            n.setX(currentX);

            //Stagger the y coords
            float y = slots[jStartHour] * 800f;
            if((jStartHour & 1) == 0){
                y += 400f;
            }
            slots[jStartHour] ++;
            n.setY(y);

            nodes.add(n);

        }

        //Create an edge for each redeployment
        for(Redeployment r : rVProj.getRedeployments()){
            //Build the edge
            EdgeDraft e = containerLoader.factory().newEdgeDraft();
            //This is simple because journey and their nodes have the same index
            e.setSource(nodes.get(r.getFromJourney().getIndex()));
            e.setTarget(nodes.get(r.getToJourney().getIndex()));
            e.setLabel(r.toString());

            if(r.getType() == RedeploymentType.ECS){
                e.setColor(Color.orange);
            } else {
                e.setColor(Color.BLUE);
            }

            //Set table attributes
            e.setValue("arc_index", r.getArcIndex());
            e.setValue("from_journey", r.getFromJourney().getJourneyCode());
            e.setValue("from_loc", r.getFromJourney().getEndLoc());
            e.setValue("to_journey", r.getToJourney().getJourneyCode());
            e.setValue("to_loc", r.getToJourney().getStartLoc());
            e.setValue("redeployment_type", r.getType().toString());
            e.setValue("equipment", r.getFromJourney().getEquip());
            e.setValue("no_paths", 0);

            e.setWeight(0.1);
            edges.add(e);
        }

        //Now add the nodes to the graph
        for(NodeDraft n : nodes) {
            containerLoader.addNode(n);
        }

        //Now add the edges to the graph
        for(EdgeDraft e : edges) {
            containerLoader.addEdge(e);
        }

    }

    public void setProject(RouteVisProject p) {
        this.rVProj = p;
    }

    @Override
    public String getName() {
        return "Timeline Generator";
    }

    @Override
    public GeneratorUI getUI() {
        return null;
    }

    @Override
    public boolean cancel() {
        cancel = true;
        return true;
    }

    @Override
    public void setProgressTicket(ProgressTicket progressTicket) {
        this.progress = progressTicket;
    }
}
