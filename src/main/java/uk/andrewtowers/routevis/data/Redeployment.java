package uk.andrewtowers.routevis.data;

import org.gephi.graph.api.Edge;
import uk.andrewtowers.routevis.app.App;

public class Redeployment {

    //The two journeys connected
    private Journey fromJourney;
    private Journey toJourney;

    private RedeploymentType type;
    private int arc_index;
    private int edge_index;

    public int noPaths;

    private int equipType;

    public int getArcIndex() {
        return arc_index;
    }

    public Edge getEdge(App app){
        return  app.graphDrawer.getEdgeFromArcIndex(arc_index);
    }

    public int getEdgeIndex(){
        return arc_index;
    }

    public void setEdge_index(int i){
        edge_index = i;
    }

    public Redeployment(Journey fromJourney, Journey toJourney, RedeploymentType type, int arc_index, int equip) {
        this.fromJourney = fromJourney;
        this.toJourney = toJourney;
        this.arc_index = arc_index;
        this.noPaths = 0;

        this.equipType = equip;

        if(!fromJourney.getClass().equals(toJourney.getClass())){
            //We dont actually do anything about this
            System.err.println("To and from journey equipment mismatch!");
        }

        this.type = type;
    }

    @Override
    public String toString(){
        return "(" + arc_index + " Redeployment)" + fromJourney.getJourneyCode() + " - " + toJourney.getJourneyCode() + " (" + fromJourney.getEndLoc() + " -> " + toJourney.getStartLoc() + ")";
    }

    public Journey getFromJourney() {
        return fromJourney;
    }

    public Journey getToJourney() {
        return toJourney;
    }

    public RedeploymentType getType() {
        return type;
    }

}
