package uk.andrewtowers.routevis.data;

import uk.andrewtowers.routevis.app.App;

import java.awt.*;
import java.util.ArrayList;

public class Journey {

    private String journeyCode;
    //Start and end location
    private String startLoc;
    private String endLoc;
    //Start and end time
    private int[] startTime;
    private int[] endTime;
    //The type of vehicle required
    private int equip;
    //The index of the journey
    private int index;

    private Point pos;

    public int getIndex() {
        return index;
    }

    //Constructor
    public Journey(String journeyCode, String startLoc, String endLoc, int startTimeHrs, int startTimeMins, int endTimeHrs, int endTimeMins, int equip, int index) {
        this.journeyCode = journeyCode;
        this.startLoc = startLoc;
        this.endLoc = endLoc;
        this.startTime = new int[]{startTimeHrs, startTimeMins};
        this.endTime = new int[]{endTimeHrs, endTimeMins};;
        this.equip = equip;
        this.index = index;
    }

    @Override
    public String toString(){
        return String.format("(%d Journey) %s: %s %04d -to- %s %04d", this.index, this.journeyCode, this.startLoc, getStartInMins(), this.endLoc, getStartInMins());
    }

    public String getFriendlyString(){
        if(this.journeyCode.equalsIgnoreCase("source")){
            return "Source";
        } else if (this.journeyCode.equalsIgnoreCase("sink")){
            return "Sink";
        }
        return this.startLoc + " - " + this.endLoc;
    }

    //Get the list of paths this journey is in
    public Path[] getPathIn(App app){
        ArrayList<Path> paths = new ArrayList();

        for(Path p : app.rtProj.getPaths()){
            for(Journey j : p.getJourneys()){
                if(j.getJourneyCode().equalsIgnoreCase(this.getJourneyCode())){
                    paths.add(p);
                    break;
                }
            }
        }

        if(paths.size() > 0){
            return paths.toArray(new Path[0]);
        } else {
            return null;
        }
    }

    public String getJourneyCode() {
        return journeyCode;
    }

    public String getStartLoc() {
        return startLoc;
    }

    public String getEndLoc() {
        return endLoc;
    }

    public int getStartInMins(){
        return (startTime[0]*100) + startTime[1];
    }

    public int getEndInMins(){
        return (endTime[0]*100) + endTime[1];
    }

    public int[] getStartTime() {
        return startTime;
    }

    public int[] getEndTime() {
        return endTime;
    }

    public int getEquip() {
        return equip;
    }

    public Point getPos() {
        return pos;
    }

    public void setPos(Point pos) {
        this.pos = pos;
    }
}
