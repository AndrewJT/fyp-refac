package uk.andrewtowers.routevis.app;

import org.gephi.graph.api.*;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.importer.api.Report;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.io.processor.spi.Processor;
import org.gephi.preview.api.*;
import org.gephi.preview.types.DependantColor;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.gephi.toolkit.demos.plugins.preview.PreviewSketch;
import uk.andrewtowers.routevis.data.*;

import java.awt.*;

public class GraphDrawer {

    private App app;

    private Column arc_indexCol;
    public Column no_paths;

    public Edge getEdgeFromArcIndex(int index){
        for(Edge e : Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getEdges()){
            if((int)e.getAttribute(arc_indexCol) == index){
                return e;
            }
        }

        System.err.println("Could not find edge with arc_index " + index);
        return null;
    }

    public void generateNoPaths(){

        if(no_paths == null){
            System.err.println("Edge no paths not set");
            return;
        }

        Edge[] graphEdges = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getEdges().toArray();

        for(Path p : app.rtProj.getPaths()){
            for(Redeployment r : p.getRedeployments()){
                int paths = (int)graphEdges[r.getEdgeIndex()].getAttribute(no_paths) + 1;
                graphEdges[r.getEdgeIndex()].setAttribute(no_paths, paths);
            }
        }

    }

    public void hideAll(){
        hideAllNodes();
        hideAllEdges();
        app.windowUI.clearPathsTable();
    }

    public void hideAllEdges(){

        for(Edge e : Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getEdges()){
            e.setAlpha(0f);
            e.setWeight(0.1f);
        }
        app.previewController.refreshPreview();
    }

    public void hideAllNodes(){

        //for (Node n: dg.getNodes()){  <-- doesn't work
        for(Node n: Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getNodes()){
            n.setAlpha(0f);
        }
        app.previewController.refreshPreview();
    }

    public void drawMainGraph(RouteVisProject proj, Workspace ws){

        //Generate graph
        Container container = Lookup.getDefault().lookup(Container.Factory.class).newContainer();
        Report report = new Report();
        container.setReport(report);
        RouteGraphGenerator routeGraphGenerator = new RouteGraphGenerator();
        routeGraphGenerator.setProject(proj);
        routeGraphGenerator.generate(container.getLoader());

        //Put into graph
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);
        importController.process(container, new DefaultProcessor(), ws);

        //Setup previewer
        app.previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel = app.previewController.getModel();
        previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
        Font defaultFont = (Font)previewModel.getProperties().getValue(PreviewProperty.NODE_LABEL_FONT);
        Font font = new Font(defaultFont.getFontName(), Font.PLAIN, 5);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, font);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_SHOW_BOX, Boolean.TRUE);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_BOX_COLOR, new DependantColor(Color.BLACK));
        previewModel.getProperties().putValue(PreviewProperty.NODE_PER_NODE_OPACITY, Boolean.TRUE);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_PROPORTIONAL_SIZE, Boolean.FALSE);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));
        previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
        previewModel.getProperties().putValue(PreviewProperty.EDGE_OPACITY, 50);
        previewModel.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(EdgeColor.Mode.ORIGINAL));
        previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);


        //Setup processing target
        G2DTarget target = (G2DTarget) app.previewController.getRenderTarget(RenderTarget.G2D_TARGET);
        PreviewSketch previewSketch = new PreviewSketch(target, ws);
        app.previewController.refreshPreview();

        //Put in the panel
        app.windowUI.mainSplit.setLeftComponent(previewSketch);
        app.windowUI.mainSplit.setDividerLocation(0.8);
        previewSketch.resetZoom();

        arc_indexCol = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getEdgeTable().getColumn("arc_index");
        no_paths = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getEdgeTable().getColumn("no_paths");

        generateNoPaths();

    }

    public void groupPaths(){

        int yPos = 0;
        boolean odd = false;
        for(Path p : app.rtProj.getPaths()){
            for(Journey j : p.getJourneys()){
                //If its position is already set, do nothing
                if(j.getPos() == null){
                    int xPos = j.getStartTime()[0] * 60 + j.getStartTime()[1];
                    if(odd){
                        j.setPos(new Point(xPos, yPos + 10));
                        odd = false;
                    } else {
                        j.setPos(new Point(xPos, yPos - 10));
                        odd = true;
                    }

                }
            }
            yPos += 50;
        }

        //Now update node positions
        Node[] graphNodes = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph().getNodes().toArray();
        for(int i=0; i<graphNodes.length; i++){
            Node n = graphNodes[i];
            Journey j = app.rtProj.getJourneys()[i];

            n.setX((float)j.getPos().x * 2.5f);
            n.setY((float)j.getPos().y);
        }

        app.previewController.refreshPreview();

    }

    public GraphDrawer (App app){
        this.app = app;
    }
}
