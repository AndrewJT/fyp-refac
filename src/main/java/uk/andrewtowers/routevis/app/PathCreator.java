package uk.andrewtowers.routevis.app;

import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.RenderTarget;
import org.gephi.toolkit.demos.plugins.preview.PreviewSketch;
import org.openide.util.Lookup;
import uk.andrewtowers.routevis.data.RedeploymentType;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class PathCreator implements ActionListener {

    private App app;

    private List<Node> pathNodes;
    private List<Edge> pathEdges;

    private DirectedGraph mainGraph;

    private PathEditTable pathEditTable;

    public void addNode(Node selectedNode) {

        //Get this node in the main workspace
        Node n = mainGraph.getNode(selectedNode.getId());

        if(pathNodes.size() < 1){
            //This is the first node. add it
            pathEditTable.drawTable();
            pathNodes.add(n);

            String title = n.getAttribute("start_loc") + " - " + n.getAttribute("end_loc");
            pathEditTable.addFirstRow(title, (Integer)n.getAttribute("start_time"), (Integer)n.getAttribute("end_time"));
            showPath();
            showCompatible(AddedDirection.BOTH);

        } else if (isStartAdjacent(n)){
            //Add edge connecting this node to the start node
            Edge e = mainGraph.getEdge(n, pathNodes.get(0));
            pathEdges.add(0,e);
            //Add this node to the start
            pathNodes.add(0, n);

            String title = n.getAttribute("start_loc") + " - " + n.getAttribute("end_loc");
            int startTime = (Integer)n.getAttribute("start_time");
            int endTime = (Integer)n.getAttribute("end_time");
            RedeploymentType type = RedeploymentType.typeFromString((String)e.getAttribute("redeployment_type"));
            pathEditTable.addRowToStart(title, startTime, endTime, type);
            showPath();
            showCompatible(AddedDirection.BACKWARD);

        } else if (isEndAdjacent(n)){
            //Add connecting edge
            Edge e = mainGraph.getEdge(pathNodes.get(pathNodes.size()-1), n);
            pathEdges.add(e);
            pathNodes.add(n);

            String title = n.getAttribute("start_loc") + " - " + n.getAttribute("end_loc");
            pathEditTable.addRowToEnd(title, (Integer)n.getAttribute("start_time"), (Integer)n.getAttribute("end_time"), RedeploymentType.typeFromString((String)e.getAttribute("redeployment_type")));
            showPath();
            showCompatible(AddedDirection.FORWARD);

        } else {
            app.windowUI.statusBar.setText("Couldn't add that node to the path!");
            return;
        }

        ((PreviewSketch)app.windowUI.mainSplit.getLeftComponent()).resetZoom();

    }


    private void showPath(){

        app.projectController.openWorkspace(app.ws1);

        for(Node n : mainGraph.getNodes()){

            if(pathNodes.contains(n)){
                //Is in the list. Show it
                n.setColor(Color.GREEN);
                n.setZ(10f);
            } else {
                //Hide those no in the set
                n.setColor(app.bkgColour);
                n.setZ(-1f);
            }
        }

        for(Edge e : mainGraph.getEdges()){

            if(pathEdges.contains(e)){
                //Is in the list. Show it
                e.setAlpha(1f);
                e.setWeight(5f);
                e.setColor(Color.GREEN);

            } else {
                //Hide those no in the set
                e.setAlpha(0.001f);
                e.setWeight(0.001f);
                e.setColor(app.bkgColour);
            }
        }

        app.previewController.refreshPreview();
    }

    private void showCompatible(AddedDirection direction){

        //Show journeys that can be added onto the end
        Node last = pathNodes.get(pathNodes.size()-1);
        List<Node> endOptions = new ArrayList<>();
        for(Edge e : mainGraph.getOutEdges(last)){
            //Show this edge and the node incident as compatible to be added to the end
            Node n = e.getTarget();
            e.setAlpha(1f);
            e.setWeight(5f);
            e.setColor(Color.BLUE);
            n.setColor(Color.BLUE);
            n.setZ(10f);
            endOptions.add(n);
        }

        List<Node> startOptions = new ArrayList<>();

        Node first = pathNodes.get(0);
        for(Edge e : mainGraph.getInEdges(first)){
            //Show this edge and the node incident as compatible to be added to the start
            Node n = e.getSource();
            e.setAlpha(1f);
            e.setWeight(5f);
            e.setColor(Color.BLUE);
            n.setColor(Color.BLUE);
            n.setZ(10f);
            startOptions.add(n);
        }

        app.previewController.refreshPreview();
    }

    //Can new node be added to the end of the list
    private boolean isEndAdjacent(Node n){
        return mainGraph.isAdjacent(pathNodes.get(pathNodes.size()-1), n);
    }

    //Can new node be added to the start of the list
    private boolean isStartAdjacent(Node n){
        return mainGraph.isAdjacent(n, pathNodes.get(0));
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        if(app.getMode() == AppMode.VIEWING){
            //Start drawing path
            app.windowUI.statusBar.setText("Select a node to start on...");
            app.setMode(AppMode.PATHCREATOR);
            reset();
            mainGraph = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1).getDirectedGraph();

        } else if (app.getMode() == AppMode.PATHCREATOR){
            //Save path
            app.windowUI.statusBar.setText("Saving...");
            app.setMode(AppMode.VIEWING);

        } else if (app.getMode() == AppMode.NONE){
            //Can't do anything
            app.windowUI.statusBar.setText("You must have a project loaded to create a path!");
        }

    }

    public void reset(){
        pathNodes = new ArrayList<Node>();
        pathEdges = new ArrayList<Edge>();

        pathEditTable = new PathEditTable(app);
    }

    public PathCreator(App app){
        this.app = app;
        reset();
    }
}
