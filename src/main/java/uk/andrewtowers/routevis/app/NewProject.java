package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.RouteVisProject;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;

public class NewProject implements ActionListener {

    private App app;

    //When the user is trying to make a new project
    public void actionPerformed(ActionEvent actionEvent) {

        //Prompt the user to select journey and redeployment files
        Boolean tryInput = true;

        String prev = app.mainPrefs.get("lastJourneyFile", "");

        RouteVisProject newProject = new RouteVisProject(app);     //The new project we are trying to create

        //Try and get journey file
        while(tryInput){
            JFileChooser journeyChooser = new JFileChooser();
            //Auto reselect previously used file
            if(prev != ""){
                journeyChooser.setSelectedFile(Paths.get(prev).toFile());
            }
            journeyChooser.addChoosableFileFilter(new FileNameExtensionFilter("NODE_INPUT (.dat)", "dat"));
            journeyChooser.setAcceptAllFileFilterUsed(false);

            int i = journeyChooser.showOpenDialog(app.mainFrame);

            if(i == JFileChooser.APPROVE_OPTION){   //User has selected a file; try and process

                File f = journeyChooser.getSelectedFile();
                if(JourneyImporter.importJourney(newProject, f)){  //Try and import this file into the project
                    app.mainPrefs.put("lastJourneyFile", f.getAbsolutePath());  //Update the last selected file pref
                    JOptionPane.showMessageDialog(app.mainFrame, "Nodes loaded successfully. Now import arcs(redeployments) and paths", "Success", JOptionPane.INFORMATION_MESSAGE);
                    tryInput = false;

                } else {
                    //Invalid file. Let user try again
                    JOptionPane.showMessageDialog(app.mainFrame, "Invalid node (journey) file!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {    //User hasn't selected a file, give up
                return;
            }
        }

        //Now our new project is ready to use

        //Now import a redeployments file
        tryInput = true;
        prev = app.mainPrefs.get("lastRedepFile", "");

        while(tryInput){
            JFileChooser redepChooser = new JFileChooser();
            //Auto reselect previously used file
            if(prev != ""){
                redepChooser.setSelectedFile(Paths.get(prev).toFile());
            }
            redepChooser.addChoosableFileFilter(new FileNameExtensionFilter("*ARC_INPUT (.dat)", "dat"));
            redepChooser.setAcceptAllFileFilterUsed(false);

            int i = redepChooser.showOpenDialog(app.mainFrame);

            if(i == JFileChooser.APPROVE_OPTION){   //User has selected a file; try and process

                File f = redepChooser.getSelectedFile();
                if(RedeploymentImporter.importRedeploymentDat(newProject, f)){  //Try and import this file into the project
                    app.mainPrefs.put("lastRedepFile", f.getAbsolutePath());  //Update the last selected file pref
                    newProject.setRedeploymentFile(f);
                    JOptionPane.showMessageDialog(app.mainFrame, "Redeployments loaded. Now import the paths file", "Success", JOptionPane.INFORMATION_MESSAGE);
                    tryInput = false;

                } else {
                    //Invalid file. Let user try again
                    JOptionPane.showMessageDialog(app.mainFrame, "Invalid node (journey) file!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {    //User hasn't selected a file, give up
                return;
            }
        }

        app.rtProj = newProject;

        //app.graphDrawer.drawMainGraph(newProject, app.ws1);
        app.setMode(AppMode.VIEWING);

    }

    public NewProject(App app){
        this.app = app;
    }
}
