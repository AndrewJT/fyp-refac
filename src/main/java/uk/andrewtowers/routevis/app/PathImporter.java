package uk.andrewtowers.routevis.app;

import sun.awt.X11.XSystemTrayPeer;
import uk.andrewtowers.routevis.data.Journey;
import uk.andrewtowers.routevis.data.Path;
import uk.andrewtowers.routevis.data.Redeployment;
import uk.andrewtowers.routevis.data.RouteVisProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PathImporter {

    public static boolean importPath(RouteVisProject project, File f, App app) {

        Boolean validFile = false;

        try (Scanner s = new Scanner(f, StandardCharsets.UTF_8.name())) {

            List<Path> pathList = new ArrayList();
            String next;

            while (s.hasNext()) {


                next = s.next();

                if (next.startsWith("LastOptPaths:")) {

                    validFile = true;

                    //We now start reading path data
                    next = s.nextLine();
                    while (!next.startsWith("]")) {   //Keep reading lines until ] is reached

                        System.out.println("Reading line: " + next);

                        if (next.startsWith("(")) {   //Lines defining new paths start with (

                            List<Journey> journeyList = new ArrayList();
                            List<Redeployment> redepList = new ArrayList();

                            //Each path is represented by two lines in the file
                            //Split the first line into 4 parts
                            String[] line1 = next.split("\\[\\[|\\]\\s\\[|\\s\\[|\\]\\s");

                            //The second part is the list of journeys
                            String[] journeyNos = line1[1].split(" ");
                            //There is an error, in some later iterations where this is reversed
                            Boolean reversed = false;
                            if (!journeyNos[0].equalsIgnoreCase("1")) {
                                reversed = true;
                            }
                            //Now create a list of journey objects from this list
                            for (String jStr : journeyNos) {
                                try {
                                    int jNo = Integer.parseInt(jStr);
                                    //Get actual journey
                                    Journey j = project.getJourneys()[jNo - 1];
                                    journeyList.add(j);

                                } catch (Exception e) {
                                    System.err.println("Could not parse journey number: " + jStr);
                                    return false;
                                }
                            }

                            //The third part of line 1 is a list of redeployments
                            String[] redepNos = line1[2].split(" ");
                            for (String rStr : redepNos) {
                                try {
                                    int rNo = Integer.parseInt(rStr);
                                    //Get an actual redeployment
                                    Redeployment r = project.getRedeployments()[rNo - 1];
                                    redepList.add(r);
                                } catch (Exception e) {
                                    System.err.println("Could not parse redeployment number: " + rStr);
                                    return false;
                                }
                            }

                            //We can also extract the vehicle unit class from the path file
                            int equipment;
                            try {
                                equipment = Integer.parseInt(line1[3].split("\\'|\\s")[1]);
                            } catch (Exception e) {
                                System.err.println("Could not parse equipment number");
                                e.printStackTrace();
                                return false;
                            }

                            //Fix the reversal if needed
                            if (reversed) {
                                Collections.reverse(journeyList);
                            }

                            //As paths take up two lines, we must consume the second
                            s.nextLine();

                            //Create a path object for this path & add to the list
                            Path p = new Path(journeyList.toArray(new Journey[0]), redepList.toArray(new Redeployment[0]), equipment, app);
                            System.out.println("Adding path " + p.toString());
                            project.addPath(p);
                        }
                        next = s.nextLine();
                    }

                    app.graphDrawer.drawMainGraph(project, app.ws1);
                    //Draw all the paths
                    app.graphDrawer.hideAll();
                    for(Path p : app.rtProj.getPaths()){
                        p.drawPath(app);
                    }
                    //Group all paths
                    app.graphDrawer.groupPaths();
                }
            }

            if (!validFile) {
                System.err.println("Invalid path file: File contains no paths!");
                return false;
            }

            return true;

        } catch (FileNotFoundException e) {
            System.err.println("Path file not found!");
            return false;
        }

    }

}
