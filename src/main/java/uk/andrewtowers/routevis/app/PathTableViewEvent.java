package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.Path;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PathTableViewEvent implements ActionListener {

    private App app;

    public PathTableViewEvent(App app) {
        this.app = app;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        //We want to draw only the path selected
        //Get path number
        int pathNo = ((TableButtonEditor)actionEvent.getSource()).getLastRowNo();

        app.graphDrawer.hideAll();

        Path p = app.rtProj.getPaths()[pathNo];

        if(p == null){
            System.err.println("Path not found");
            return;
        }

        p.drawPath(app);
    }
}
