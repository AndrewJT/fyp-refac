package uk.andrewtowers.routevis.app;

public enum AddedDirection {
    BACKWARD,
    BOTH,
    FORWARD
}
