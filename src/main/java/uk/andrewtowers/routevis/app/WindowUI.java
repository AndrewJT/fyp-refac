package uk.andrewtowers.routevis.app;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class WindowUI {

    private App app;

    public JMenuBar mainMenuBar;
    public JToolBar mainToolBar;

    public JButton newPath;

    public JPanel mainPanel;

    public JSplitPane mainSplit;

    public StatusBar statusBar;

    public DefaultTableModel pathsTableModel;
    public JTable pathsTable;
    public JScrollPane tableScroll;




    //Create top menu bar
    private void setUpMenuBar(){

        mainMenuBar = new JMenuBar();

        //Menus
        JMenu file, view, help;

        //Setup file menu
        file = new JMenu("File");
        JMenuItem loadNew = new JMenuItem("New");
        JMenuItem loadPathsButton = new JMenuItem("Import Paths");
        JMenuItem loadExisting = new JMenuItem("Load");
        JMenuItem saveCurrent = new JMenuItem("Save");
        JMenuItem export = new JMenuItem("Export for Gephi");

        //For creating a new project from raw data
        NewProject newProject = new NewProject(app);
        loadNew.addActionListener(newProject);
        file.add(loadNew);

        LoadPaths loadPaths = new LoadPaths(app);
        loadPathsButton.addActionListener(loadPaths);
        file.add(loadPathsButton);

        file.add(loadExisting);
        file.add(saveCurrent);

        export.addActionListener(new Exporter(app));
        file.add(export);

        //Setup view menu
        view = new JMenu("View");

        ViewMenuEvents viewMenuEvents = new ViewMenuEvents(app);

        JMenuItem allPaths = new JMenuItem("Show all paths");
        allPaths.setActionCommand("view_paths");
        allPaths.addActionListener(viewMenuEvents);
        view.add(allPaths);

        JMenuItem groupPaths = new JMenuItem("Group Paths");
        groupPaths.setActionCommand("group_paths");
        groupPaths.addActionListener(viewMenuEvents);
        view.add(groupPaths);

        JMenuItem noPaths = new JMenuItem("Show no paths");
        noPaths.setActionCommand("view_none");
        noPaths.addActionListener(viewMenuEvents);
        view.add(noPaths);

        //Setup help menu
        help = new JMenu("Help");
        JMenuItem aboutApp = new JMenuItem("About");

        help.add(aboutApp);

        //Add menus to bar
        mainMenuBar.add(file);
        mainMenuBar.add(view);
        mainMenuBar.add(help);

    }

    private void setUpToolbar(){
        mainToolBar = new JToolBar("Toolbar");
        newPath = new JButton("Create Path");
        newPath.addActionListener(app.pathCreator);

        mainToolBar.add(newPath);
    }

    //These panels will hold the preview widgets
    private void setUpPanels(){
        mainPanel = new JPanel();

        mainSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, tableScroll);
    }

    public void setUpTable(){
        pathsTableModel = new DefaultTableModel();
        pathsTableModel.addColumn("#");
        pathsTableModel.addColumn("Journeys");
        pathsTableModel.addColumn("ECS");
        pathsTableModel.addColumn("View");

        pathsTable = new JTable(pathsTableModel);

        //Set up view column
        TableButtonEditor viewEditor = new TableButtonEditor(new JCheckBox(), app);
        viewEditor.addActionListener(new PathTableViewEvent(app));

        pathsTable.getColumn("View").setCellRenderer(new TableButtonRenderer());
        pathsTable.getColumn("View").setCellEditor(viewEditor);

        pathsTable.setFillsViewportHeight(true);

        tableScroll = new JScrollPane(pathsTable);
    }

    public void clearPathsTable(){

        if(pathsTableModel.getRowCount() > 0){
            for (int i = pathsTableModel.getRowCount()-1; i >-1; i-- ){
                pathsTableModel.removeRow(i);
            }
        }

    }

    public WindowUI(App app){

        //Set up necessary UI elements for the main app
        this.app = app;
        setUpMenuBar();
        setUpToolbar();
        setUpTable();
        setUpPanels();

        statusBar = new StatusBar();
    }

}
