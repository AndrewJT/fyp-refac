package uk.andrewtowers.routevis.app;

import org.apache.commons.io.FilenameUtils;
import uk.andrewtowers.routevis.data.RouteVisProject;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;

public class LoadPaths implements ActionListener {

    private App app;

    public LoadPaths(App app) {
        this.app = app;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {


        Boolean tryInput = true;

        String prev = app.mainPrefs.get("lastPathsFile", "");
        RouteVisProject routeVisProject = app.rtProj;

        if(routeVisProject == null){
            System.err.println("Cannot add paths without project!");
            JOptionPane.showMessageDialog(app.mainFrame, "You must have an active project to load paths into", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        while(tryInput){

            JFileChooser pathFileChooser = new JFileChooser();

            //Auto reselect previously used file
            if(prev != ""){
                pathFileChooser.setSelectedFile(Paths.get(prev).toFile());
            }

            String it_id = routeVisProject.getRedeploymentFile().getName().replaceAll("ARC_INPUT.dat", "");
            String filenames = it_id + "lastOptArcs.xml";

            pathFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(filenames,"xml"));
            pathFileChooser.setAcceptAllFileFilterUsed(false);

            int i = pathFileChooser.showOpenDialog(app.mainFrame);

            if(i == JFileChooser.APPROVE_OPTION){

                File f = pathFileChooser.getSelectedFile();

                String ext = FilenameUtils.getExtension(f.getName());

                if(f.getName().startsWith(it_id)){
                    if(ext.equalsIgnoreCase("xml")){
                        if(ArcPathXmlImporter.importFile(app, f)){
                            app.mainPrefs.put("lastPathsFile", f.getAbsolutePath());  //Update the last selected file pref
                            tryInput = false;
                        } else {
                            JOptionPane.showMessageDialog(app.mainFrame, "Invalid paths file!", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(app.mainFrame, "The path file should start with: " + it_id, "Error", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                return;
            }

        }
    }

}
