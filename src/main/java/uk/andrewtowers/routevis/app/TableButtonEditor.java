package uk.andrewtowers.routevis.app;

//Essentially lifted from http://www.java2s.com/Code/Java/Swing-Components/ButtonTableExample.htm

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class TableButtonEditor extends DefaultCellEditor {
    protected JButton button;

    private String label;
    private int rowNo;

    private boolean isPushed;

    private EventListenerList listenerList;

    private App app;

    public void addActionListener(ActionListener listener) {
        listenerList.add(ActionListener.class, listener);
    }

    public void removeActionListener(ActionListener listener) {
        listenerList.remove(ActionListener.class, listener);
    }

    public int getLastRowNo() {
        return rowNo;
    }

    public TableButtonEditor(JCheckBox checkBox, App app) {
        super(checkBox);
        this.listenerList = new EventListenerList();
        this.app = app;
        button = new JButton();
        button.setOpaque(true);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        if (isSelected) {
            button.setForeground(table.getSelectionForeground());
            button.setBackground(table.getSelectionBackground());
        } else {
            button.setForeground(table.getForeground());
            button.setBackground(table.getBackground());
        }
        label = (value == null) ? "" : value.toString();
        rowNo = row;
        button.setText(label);
        isPushed = true;
        return button;
    }

    public Object getCellEditorValue() {
        if (isPushed) {

            //Fire event
            ActionEvent event = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, label);
            ActionListener[] listeners = listenerList.getListeners(ActionListener.class);
            if(listeners != null){
                for(ActionListener l : listeners){
                    l.actionPerformed(event);
                }
            }
        }
        isPushed = false;
        return new String(label);
    }

    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
}