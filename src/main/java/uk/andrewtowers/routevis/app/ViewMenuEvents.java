package uk.andrewtowers.routevis.app;

import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.openide.util.Lookup;
import uk.andrewtowers.routevis.data.Path;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewMenuEvents implements ActionListener {

    private App app;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        GraphModel gm = Lookup.getDefault().lookup(GraphController.class).getGraphModel(app.ws1);

        if(actionEvent.getActionCommand().equalsIgnoreCase("view_none")){
            app.graphDrawer.hideAll();
        } else if (actionEvent.getActionCommand().equalsIgnoreCase("view_paths")){
            app.graphDrawer.hideAll();
            app.windowUI.clearPathsTable();
            for(Path p : app.rtProj.getPaths()){
                p.drawPath(app);
            }
        }else if (actionEvent.getActionCommand().equalsIgnoreCase("group_paths")){
            app.graphDrawer.groupPaths();
        }
    }

    public ViewMenuEvents(App app){
        this.app = app;
    }
}
