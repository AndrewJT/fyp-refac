package uk.andrewtowers.routevis.app;

import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.spi.GraphExporter;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import uk.andrewtowers.routevis.data.RouteGraphGenerator;
import uk.andrewtowers.routevis.data.RouteVisProject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Exporter implements ActionListener {

    private App app;

    //For exporting as gef
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        //Export as gexf
        ExportController ec = Lookup.getDefault().lookup(ExportController.class);
        GraphExporter exporter = (GraphExporter) ec.getExporter("gexf");
        exporter.setWorkspace(app.ws1);

        try {

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Export as GEXF");
            File ogFIle = new File("io_gexf.gexf");
            fileChooser.setSelectedFile(ogFIle);
            int selection = fileChooser.showSaveDialog(app.mainFrame);

            if(selection == JFileChooser.APPROVE_OPTION){
                ec.exportFile(fileChooser.getSelectedFile(), exporter);
           }

        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }


    }

    public Exporter(App app){
        this.app = app;
    }
}
