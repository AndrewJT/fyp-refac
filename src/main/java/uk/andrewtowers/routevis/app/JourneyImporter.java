package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.Journey;
import uk.andrewtowers.routevis.data.RouteVisProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JourneyImporter {

    //A Parser for importing journey files
    public static Boolean importJourney(RouteVisProject project, File f){
        //Scanner will through error if file does not exist
        try(Scanner s = new Scanner(f, StandardCharsets.UTF_8.name())){
            //Timeset: Defines time format. Will usually be hour:minute
            String[] timeSet = new String[2];
            //Define an empty Journey arraylist
            List<Journey> journeyList = new ArrayList();
            //Import mode for keeping whether we are parsing nodes or setting time format
            NodeInputMode mode = NodeInputMode.NONE;
            //Loop through all symbols in file
            while(s.hasNext()){

                String next = s.next();
                if(mode == NodeInputMode.NONE){
                    //Must find out what node to transition into next
                    if(next.startsWith("TimeSet:")){
                        //Load timeset information
                        timeSet[0] = s.next().replaceAll("[^a-zA-Z0-9]", "");
                        timeSet[1] = s.next().replaceAll("[^a-zA-Z0-9]", "");
                        System.out.println("Using time: " + timeSet[0] + " : " + timeSet[1]);
                        mode = NodeInputMode.NONE;

                    } else if(next.startsWith("OriginalNODE:")){
                        //Start reading node data
                        mode = NodeInputMode.ORIGINALNODE;
                    }

                } else if (mode == NodeInputMode.ORIGINALNODE){
                    //Reading node data
                    String line = s.nextLine();
                    //Keep reading lines until ] is reached
                    while(!(line.startsWith("]"))){
                        //Journeys are in the format:
                        //(index) ['traincode' '' '' 'startlocation' 'P' [start time] * 'endlocation' 'P' [endtime] * 0 [vehicleclass] .... ]
                        //All journey lines start with a (
                        if(line.startsWith("(")){
                            //The following code throws errors if Integer.parse fails
                            try{
                                //Split the line using a regular expression
                                String[] comp = line.split("\\s'|\\s\\[|\\]\\s");

                                //Journey index. Note: These are indexed starting at 1
                                String index = comp[0].replaceAll("\\D+","");
                                int intindex = Integer.parseInt(index);

                                String jouneyCode = comp[1].replaceAll("[\\[|\\'|\\]]", "");

                                String startLoc = comp[4].replaceAll("'", "");
                                String[] startTime = comp[6].replaceAll("[\\[|\\]]", "").split(" ");
                                int startHour = Integer.parseInt(startTime[0]);
                                int startMin = Integer.parseInt(startTime[1]);

                                String endLoc = comp[8].replaceAll("'", "");
                                String[] endTime = comp[10].replaceAll("[\\[|\\]]", "").split("  ");
                                int endHour = Integer.parseInt(endTime[0]);
                                int endMin = Integer.parseInt(endTime[1]);

                                int equipClass = Integer.parseInt(comp[12].replaceAll("[\\[|\\]|\\'|\\s]", ""));

                                //Create a journey object and add it to the list
                                Journey j = new Journey(jouneyCode, startLoc, endLoc, startHour, startMin, endHour, endMin, equipClass, intindex-1);
                                journeyList.add(j);

                            } catch (Exception e){
                                System.err.println("Could not parse journey node data");
                                e.printStackTrace();
                                return false;
                            }
                        }
                        //Move on to the next line
                        line = s.nextLine();
                    }
                    //Done getting nodes
                    mode = NodeInputMode.NONE;
                }
            }

            //Now we have all the journeys as a list and the timeset value, we can add them to the project and return
            project.setTimeSet(timeSet);
            project.setJourneys(journeyList.toArray(new Journey[0]));
            return true;

        } catch (FileNotFoundException e){
            System.err.println("Journey file not found!");
            return false;
        }
    }

    //Each one is a type of data available in the file
    public enum NodeInputMode { //Note: most of these aren't used
        NONE,
        TIMESET,
        ORIGINALNODE,
        EXISTARC
    }
}
