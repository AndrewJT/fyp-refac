package uk.andrewtowers.routevis.app;

public enum AppMode {
    NONE,
    VIEWING,
    PATHCREATOR
}
