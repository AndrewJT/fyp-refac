package uk.andrewtowers.routevis.app;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

//This exists to render buttons onto tables
//For some reason this isn't built in
//Essentially lifted from http://www.java2s.com/Code/Java/Swing-Components/ButtonTableExample.htm

public class TableButtonRenderer extends JButton implements TableCellRenderer {

    public TableButtonRenderer(){
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object val, boolean isSelected, boolean hasFocus, int row, int col) {

        if(isSelected){
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(UIManager.getColor("Button.background"));
        }

        if(val == null){
            setText("");
        } else {
            setText(val.toString());
        }

        return this;
    }
}
