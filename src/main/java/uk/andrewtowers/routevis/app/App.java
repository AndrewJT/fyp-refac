package uk.andrewtowers.routevis.app;

import org.gephi.preview.api.PreviewController;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import uk.andrewtowers.routevis.data.RouteVisProject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

public class App {

    private static App instance;

    public JFrame mainFrame;
    public Preferences mainPrefs;
    public WindowUI windowUI;
    public GraphDrawer graphDrawer;

    public ProjectController projectController;
    public Workspace ws1;
    public PreviewController previewController;

    public RouteVisProject rtProj;
    public PathCreator pathCreator;

    public Color bkgColour;

    private AppMode mode;

    private void openWindow(){

        //Setup gephi stuff
        projectController = Lookup.getDefault().lookup(ProjectController.class);
        projectController.newProject();
        ws1 = projectController.getCurrentWorkspace();

        //Setup main application parameters
        mainFrame = new JFrame("Route Visualiser");
        mainPrefs = Preferences.userRoot().node("uk/andrewtowers/routevis/1/prefs");
        pathCreator = new PathCreator(this);
        windowUI = new WindowUI(this);      //Used to generate UI components
        graphDrawer = new GraphDrawer(this);
        mode = AppMode.NONE;
        bkgColour = Color.WHITE;

        //Setup UI elements
        mainFrame.setJMenuBar(windowUI.mainMenuBar);
        mainFrame.setLayout(new BorderLayout());
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.add(windowUI.mainToolBar, BorderLayout.PAGE_START);
        mainFrame.add(windowUI.mainSplit, BorderLayout.CENTER);
        mainFrame.add(windowUI.statusBar, BorderLayout.PAGE_END);

        //Move to previous location
        mainFrame.setLocation(new Point(mainPrefs.getInt("WindowX", 0), mainPrefs.getInt("WindowY", 0)));
        mainFrame.setSize(mainPrefs.getInt("WindowW", 1024),mainPrefs.getInt("WindowH", 723) );

        //Window loaded event
        mainFrame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                windowUI.mainSplit.setDividerLocation(0.8);
            }
        });

        //Window close event
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {

                saveWindowPos();
                super.windowClosing(windowEvent);
            }
        });

        mainFrame.setVisible(true);

    }

    private void saveWindowPos(){
        mainPrefs.putInt("WindowX", mainFrame.getLocation().x);
        mainPrefs.putInt("WindowY", mainFrame.getLocation().y);
        mainPrefs.putInt("WindowW", mainFrame.getSize().width);
        mainPrefs.putInt("WindowH", mainFrame.getSize().height);
    }

    public AppMode getMode() {
        return mode;
    }

    public void setMode(AppMode mode) {
        this.mode = mode;
    }

    public App(){
        System.out.println("**** App Created");

        if(instance == null){
            instance = this;
        } else {
            System.err.println("Trying to make a second app instance!");
            System.exit(1);
        }
    }

    public static App getInstance(){
        return instance;
    }

    public static void main(String[] args){

        App app = new App();

        app.openWindow();

    }
}
