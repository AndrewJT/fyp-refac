package uk.andrewtowers.routevis.app;

import javax.swing.*;
import java.awt.*;

public class StatusBar extends JLabel {

    public StatusBar(){
        super();
        super.setPreferredSize(new Dimension(100, 16));
    }

}
