package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.Journey;
import uk.andrewtowers.routevis.data.RedeploymentType;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class PathEditTable {

    private App app;

    private DefaultTableModel model;
    private JTable table;
    private JScrollPane scrollTable;

    public void deleteFirst(){
        model.removeRow(0);
        model.removeRow(0);
    }

    public void deleteLast(){
        int size = model.getRowCount();
        model.removeRow(size-1);
        model.removeRow(size-2);
    }

    public void addRowToEnd(String title, int startTime, int endTime){
        Object[] row = {title, String.format("%04d", startTime), String.format("%04d", endTime), "x"};

        //Remove the delete option from the previous journey
        if(model.getRowCount() > 0){
            model.setValueAt("", 3, model.getRowCount()-1);
        }
        model.addRow(row);
    }

    public void addFirstRow(String title, int startTime, int endTime){
        Object[] row = {title, String.format("%04d", startTime), String.format("%04d", endTime), "x"};
        model.addRow(row);
    }

    public void addRowToEnd(String title, int startTime, int endTime, RedeploymentType redepType){
        Object[] row1 = {redepType.toString()};
        model.addRow(row1);

        Object[] row2 = {title, String.format("%04d", startTime), String.format("%04d", endTime), "x"};

        //Remove the delete option from the previous journey
        //model.setValueAt("asd", 1, model.getRowCount()-2);

        model.addRow(row2);
    }

    public void addRowToStart(String title, int startTime, int endTime, RedeploymentType redepType){
        //Remove delete option from first row
        //model.setValueAt("dsa", 3, 0);
        //Add row for redeployment
        Object[] row1 = {redepType.toString()};
        model.insertRow(0, row1);

        //Add row for journey
        Object[] row2 = {title, String.format("%04d", startTime), String.format("%04d", endTime), "x"};
        model.insertRow(0,row2);
    }

    public void drawTable(){
        app.windowUI.mainSplit.setRightComponent(scrollTable);
        app.windowUI.mainSplit.setDividerLocation(0.8);
    }

    public PathEditTable(App app){
        this.app = app;

        model = new DefaultTableModel();

        model.addColumn("Journey");
        model.addColumn("Start");
        model.addColumn("End");
        model.addColumn("x");

        table = new JTable(model);
        table.getColumn("x").setCellRenderer(new TableButtonRenderer());
        table.getColumn("x").setCellEditor(new TableButtonEditor(new JCheckBox(), app));

        table.setFillsViewportHeight(true);

        scrollTable = new JScrollPane(table);
    }
}
