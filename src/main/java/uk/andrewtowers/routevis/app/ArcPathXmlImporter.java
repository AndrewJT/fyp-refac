package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.*;
import org.w3c.dom.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.*;
import java.io.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ArcPathXmlImporter implements ActionListener {

    private App app;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        //Set up for import
        Boolean tryInput = true;
        String prev = app.mainPrefs.get("lastXMLImport", "");
        RouteVisProject project = app.rtProj;
        if(project == null){
            JOptionPane.showMessageDialog(app.mainFrame, "You must create a project before importing arcs and paths!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        //Keep trying for a valid input until the user says no
        while(tryInput){
            //Set up file chooser, preselecting the last used file, if there was one
            JFileChooser importChooser = new JFileChooser();
            if(prev != ""){
                importChooser.setSelectedFile(Paths.get(prev).toFile());
            }
            importChooser.addChoosableFileFilter(new FileNameExtensionFilter("*lastOptArcs.xml (.xml)", "xml"));
            importChooser.setAcceptAllFileFilterUsed(false);
            int i = importChooser.showOpenDialog(app.mainFrame);

            if(i == JFileChooser.APPROVE_OPTION){
                //The user has selected a file, try to import it

                File f = importChooser.getSelectedFile();
                if(ArcPathXmlImporter.importFile(app, f)){
                    //Import success!
                    app.mainPrefs.put("lastXMLImport", f.getAbsolutePath());  //Update the last selected file pref
                    tryInput = false;

                } else {
                    //Import failed
                    JOptionPane.showMessageDialog(app.mainFrame, "Invalid arc(redeployment)/path file!", "Error", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                //The user has decided against importing a file. Quit this
                return;
            }
        }

    }

    public static boolean importFile(App app, File file){

        RouteVisProject project = app.rtProj;
        app.windowUI.clearPathsTable();

        try {
            //Set up and parse XML file
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(file);
            doc.getDocumentElement().normalize();

            //Load data into lists
            NodeList pathDomNodes = doc.getElementsByTagName("optPathSol");

            //Make arc nodes into redeployment objects

            //Make path nodes into path object list
            List<Path> pathList = new ArrayList();
            for(int i=0; i<pathDomNodes.getLength(); i++){

                Node domNode = pathDomNodes.item(i);
                if (domNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element dElement = (Element) domNode;

                    //Get the list of journeys
                    String[] journeyNodeList = dElement.getAttribute("PathNodeList").split(",");
                    List<Journey> pathJourneyList = new ArrayList();
                    //Convert this into a list of journey objects
                    for(String j : journeyNodeList){
                        int journeyIndex = Integer.parseInt(j) - 1;
                        pathJourneyList.add(project.getJourneys()[journeyIndex]);
                    }
                    //Sometimes there is an error where the journey ist is reversed in later iterations
                    if(!journeyNodeList[0].equalsIgnoreCase("1")){
                        Collections.reverse(pathJourneyList);
                        //System.err.println("Path journey list has been reversed!");
                    }

                    //Get the list of redeployments
                    String[] redepNodeList = dElement.getAttribute("PathArcList").split(",");
                    //Convert this string list to a list of redeployments
                    List<Redeployment> pathRedepList = new ArrayList();
                    for(String r : redepNodeList){
                        int redepIndex = Integer.parseInt(r) - 1;
                        Redeployment redep = project.getRedeployments()[redepIndex];
                        if(redep == null){
                            System.err.println("Failed to add path: Referenced invalid arc (redeployment)");
                            return false;
                        }
                        pathRedepList.add(redep);
                    }
                    //Sometimes this is also backwards
                    if(pathRedepList.get(0).getType() != RedeploymentType.SIGNON){
                        //The 1st redeployment isn't a sign on. This is probably backwards
                        Collections.reverse(pathRedepList);
                        //System.err.println("Path redeployment list has been reversed!");
                    }

                    //Get other path attributes
                    int noUnits = Integer.parseInt(dElement.getAttribute("NumberUnits"));

                    //Get vehicle type list
                    String[] pathTypeList = dElement.getAttribute("PathTypeList").split(",");
                    int[] typeList = new int[pathTypeList.length];
                    for(int j=0; j<pathTypeList.length; j++){
                        typeList[j] = Integer.parseInt(pathTypeList[j]);
                    }

                    //Collate journey and redeployment list to form a path
                    Path path = new Path(pathJourneyList.toArray(new Journey[0]), pathRedepList.toArray(new Redeployment[0]), noUnits, typeList, app);
                    project.addPath(path);
                    System.out.println("Added path " + path.toString());
                }
            }
            app.graphDrawer.drawMainGraph(project, app.ws1);
            //Draw all the paths
            app.graphDrawer.hideAll();
            for(Path p : app.rtProj.getPaths()){
                p.drawPath(app);
            }
            //Group all paths
            app.graphDrawer.groupPaths();
            return true;

        } catch (Exception e){

            e.printStackTrace();
            return false;

        }

    }

    public ArcPathXmlImporter(App app) {
        this.app = app;
    }
}
