package uk.andrewtowers.routevis.app;

import uk.andrewtowers.routevis.data.Journey;
import uk.andrewtowers.routevis.data.Redeployment;
import uk.andrewtowers.routevis.data.RedeploymentType;
import uk.andrewtowers.routevis.data.RouteVisProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RedeploymentImporter {

    public static boolean importRedeployment(RouteVisProject newProject, File f) {

        if(f.exists()){

            //TODO: Sep. parser for ARC XML files
            return importRedeploymentDat(newProject, f);

        } else {
            System.err.println("Redeployment (arc) file does not exist!");
            return false;
        }

    }

    //ARC_INPUT.dat file parser
    public static boolean importRedeploymentDat(RouteVisProject newProject, File f){

        try(Scanner s = new Scanner(f, StandardCharsets.UTF_8.name())){

            String next = s.nextLine();
            List<Redeployment> redeploymentList = new ArrayList();

            if(next.startsWith("ARC:")){    //The first line should just be ARC:

                int arc_index = 0;

                while(!next.startsWith("]")){   //Keep going until the end of the list

                    if(next.startsWith("(")){   //Is this an arc

                        try{

                             String[] comp = next.split("\\(\\s|\\)\\s|\\['|'\\s|'|\\s\\[|\\]!|\\s-->\\s");

                            int arcIndex = Integer.parseInt(comp[0].replaceAll("[^0-9]", ""));
                            String startJouneyCode = comp[2];
                            String endJouneyCode = comp[4];

                            //Get start and end indices
                            String[] jouneyIndices = comp[5].split(" ");
                            int startJouneyIndex = Integer.parseInt(jouneyIndices[0]);
                            int endJouneyIndex = Integer.parseInt(jouneyIndices[1]);

                            int equipment = Integer.parseInt(comp[7]);

                            RedeploymentType t = RedeploymentType.typeFromString(comp[9]);

                            //Find start and end journeys from loaded info
                            //File starts at 1 not zero
                            Journey startJourney = newProject.getJourneys()[startJouneyIndex-1];
                            Journey endJourney = newProject.getJourneys()[endJouneyIndex-1];

                            /* Below is slow
                            Journey startJourney = newProject.getJourneyFromCode(startJouneyCode);
                            Journey endJourney = newProject.getJourneyFromCode(endJouneyCode);
                           /*

                             */
                            if(startJourney == null || endJourney == null){
                                return false;
                            }

                            //Create redeployment object
                            Redeployment r = new Redeployment(startJourney, endJourney, t, arc_index, equipment);
                            arc_index++;

                            redeploymentList.add(r);

                        } catch (Exception e){

                            System.err.println("Could not parse redeployment arc data");
                            e.printStackTrace();
                            return false;

                        }

                    }

                    next = s.nextLine();

                }

            } else {

                System.err.println("Invalid arc file!");
                return false;

            }

            //Now we have all the redeployments. Add them to the project
            newProject.setRedeployments(redeploymentList.toArray(new Redeployment[0]));
            return true;

        } catch (FileNotFoundException e){

            System.err.println("Redeployment (arc) file does not exist!");
            return false;
        }

    }

}
