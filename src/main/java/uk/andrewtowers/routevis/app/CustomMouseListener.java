package uk.andrewtowers.routevis.app;

import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewMouseEvent;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.spi.PreviewMouseListener;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import uk.andrewtowers.routevis.data.Journey;
import uk.andrewtowers.routevis.data.Path;

@ServiceProvider(service = PreviewMouseListener.class)
public class CustomMouseListener implements PreviewMouseListener {

    private App app;

    @Override
    public void mouseClicked(PreviewMouseEvent previewMouseEvent, PreviewProperties previewProperties, Workspace workspace) {

        for (Node n : Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace).getGraph().getNodes()){

            if(clickingInNode(n, previewMouseEvent)){

                if(app.getMode() == AppMode.PATHCREATOR){
                    app.pathCreator.addNode(n);
                } else {
                    //We want to just draw this nodes paths
                    String nodejcode = (String) n.getAttribute("journey_code");
                    if(nodejcode != null){
                        for(Journey j : app.rtProj.getJourneys()){
                            if(j.getJourneyCode().equalsIgnoreCase(nodejcode)){
                                //This is the node... show its paths
                                app.graphDrawer.hideAll();
                                for(Path p : j.getPathIn(app)){
                                    p.drawPath(app);
                                }
                                break;
                            }
                        }
                    }
                }


                previewMouseEvent.setConsumed(true);
                return;
            }
        }

        previewMouseEvent.setConsumed(true);

    }

    @Override
    public void mousePressed(PreviewMouseEvent previewMouseEvent, PreviewProperties previewProperties, Workspace workspace) {

    }

    @Override
    public void mouseDragged(PreviewMouseEvent previewMouseEvent, PreviewProperties previewProperties, Workspace workspace) {

    }

    @Override
    public void mouseReleased(PreviewMouseEvent previewMouseEvent, PreviewProperties previewProperties, Workspace workspace) {

    }

    private boolean clickingInNode(Node node, PreviewMouseEvent event) {
        float xdiff = node.x() - event.x;
        float ydiff = -node.y() - event.y;//Note that y axis is inverse for node coordinates
        float radius = node.size();

        return xdiff * xdiff + ydiff * ydiff < radius * radius;
    }

    public CustomMouseListener(){
        app = App.getInstance();
    }
}
